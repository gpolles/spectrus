#ifndef BETAGM_DEFINITIONS_H_
#define BETAGM_DEFINITIONS_H_

#define MAX_PROT_LEN (100000) /* Maximum allowed length for proteins to be processed */
#define BROKEN_CHAIN_CUTOFF (4.5)

#include <math.h>

struct residue{
  int residue_type; /* type of a.a. mapped to integer number from 0 to 19 (see read_pdb_file)*/ 
  double ca[3];     /* CA coordinates */
  double cb[3];     /* CA coordinates */
  double Bfactor;   /* experimental Bfactor */
  int No_CB;        /* Flag: for GLY type residue no CB construction */
  int CB_equal_CA;  /* Flag: for residues at chain termini CB coincides with CA
                       unless residue is of type GLY */
  int chainNo;

};

struct params{
double Int_Range, V_PEPT, V_CACA, V_CACB, V_CBCB;
int DUMP_EIGENVALUES, N_SLOWEST_EIGENVECT, DUMP_FULL_COVMAT;
int DUMP_REDUCED_COVMAT, DUMP_NORMALISED_REDUCED_COVMAT, DUMP_MEAN_SQUARE_DISPL;
int cgfactor;
};

inline double scal_d (double *a, double *b, int dim)
{

  int i;
  double temp;

  temp = 0.0;
  for (i = 0; i < dim; i++)
    {
      temp += a[i] * b[i];
    }
  return (temp);
}
/*******************************/

inline double norm_d (double *a, int dim)
{

  return (sqrt (scal_d (a, a, dim)));
}
/*******************************/

inline double dist_d (double *a, double *b, int dim)
{

  int i;
  double temp;

  temp = 0.0;
  for (i = 0; i < dim; i++)
    {
      temp += (a[i] - b[i]) * (a[i] - b[i]);
    }

  temp = sqrt (temp);
  return (temp);
}

#endif //BETAGM_DEFINITIONS_H_
