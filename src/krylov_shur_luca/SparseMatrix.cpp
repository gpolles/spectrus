/*
 * SparseMatrix.cpp
 *
 *  Created on: Jul 10, 2012
 *      Author: gpolles
 */
#include <cstdio>
#include <iostream>
#include <iostream>
#include <algorithm>

#include "SparseMatrix.h"

using namespace std;
namespace betagm
{

void SparseMatrix::getCsc(struct CscSparseMatrix& csc) {

  csc.dimension = static_cast<int>(_size);
  csc.numNonZero = static_cast<int>(_numNonZero);
  csc.values.reserve(_numNonZero);
  csc.irows.reserve(_numNonZero);
  csc.pcols.reserve(_size+1);

  for (int i = 0; i < static_cast<int>(_size); ++i) {
    sort(_columns[i].begin(),_columns[i].end(),customSortingFunction);
  }
  int num_ptr = 0;
  for (int i = 0; i < static_cast<int>(_size); ++i) {
    csc.pcols.push_back(num_ptr) ;
    for (int j = 0; j < static_cast<int>(_columns[i].size()); ++j) {
      csc.values.push_back(_columns[i][j].value);
      csc.irows.push_back(_columns[i][j].rowIndex);
      ++num_ptr;
    }
  }
  if(num_ptr!=(int)_numNonZero) cerr<< "WRONG"<<endl<<flush;
  csc.pcols.push_back(_numNonZero);
}

void SparseMatrix::print() {
  int prot_len = _size/3;
  for (int mu=0; mu < 3; ++mu){
    for (int nu=0; nu < 3; ++nu){
      for(int i=0; i < prot_len; i++){
        for(int j=0; j < prot_len; j++){
          if(isDefined(i+prot_len*mu,j+prot_len*nu)) printf("%4d %4d %4d %4d %e\n",i,j,mu,nu,(*this)(i+prot_len*mu,j+prot_len*nu));
        }
      }
    }
  }
}


} /* namespace betagm */
