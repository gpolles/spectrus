//============================================================================
// Author      : Guido Polles, Luca Ponzoni
// Version     :
// Copyright   : 
// Description : 
//============================================================================

//#include <mpi.h>

#include <slepceps.h>

#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <mylib/mylib.h>

extern "C"{
#include "definitions.h"
}

#include "Clock.h"

using namespace std;

static char help[] = "help msg";
static int call_number = 0;

#undef __FUNCT__
#define __FUNCT__ "main"



int krylov_shur(int n_dom_max, int prot_len, int n_eigvec, 
		      mylib::SparseMatrix<double>& interactionMatrix, vector< vector<double> >& first_eigvecs,
		      vector<int>& not_outlier_in, vector<int>& not_outlier_out, int iter) {
  
  Clock clk;
  struct params parameters;
  struct residue residues[MAX_PROT_LEN];
  
//   mylib::SparseMatrix<double> interactionMatrix;
//   interactionMatrix.ReadFromFile("nomefile.bin");
  

  call_number++;
  
  if(call_number == 1) {
    printf("  SPECTRUM OF THE LAPLACIAN (with SLEPc)\n");
    printf("  ------------------------------------------ \n");
  }
  else if(call_number == 2) {
    fprintf(stderr, "# ERROR: SLEPc called twice! \n");
    exit(1);
    // OTHERWISE, uncomment the following lines and check SlepcFinalize() at the end
//     printf("  SPECTRUM OF THE LAPLACIAN WITHOUT OUTLIERS \n");
//     printf("  ------------------------------------------ \n");
  }
  
  clk.start("Total");
  
//   read_parameters(&parameters);
//   fp = open_file_r("PDB_FILES.TXT");
//   fscanf(fp,"%s",protein_name);
//   fclose(fp);
// 
//   sprintf(pdbfile_name, "pdb_files/%s", protein_name);
//   printf("\t**** PROCESSING FILE %s ****\n\n",pdbfile_name);
//   read_pdb_file(pdbfile_name,residues ,&prot_len);
// 
//   // Construct the CB's for all sites except for chain ends and GLY's .
//   construct_cbetas(residues, prot_len);
//   SparseMatrix interactionMatrix(prot_len*3);



  
//   SparseMatrix interactionMatrix(prot_len);
// 
//   clk.start("Laplacian");
//   printf("  Building Laplacian... \n");
//   construct_Matrix_M(interactionMatrix,residues,prot_len,&parameters,
// 		     mtrx_sigma,k_neigh,k_n_neigh,k_nn,which_L,k_nn_OR,
// 		     not_outlier_in,iter);
  cout << "  > Matrix size: " << interactionMatrix.getNumNonZero()*12./1024/1024 << " MB" << endl<< flush;
//   clk.stop("Laplacian");



  // Solve the EP with SLEPc
  
  PetscInt dim=interactionMatrix.size();
  PetscInt max_row_len=0;
  PetscInt row_len[dim];
  for (size_t i = 0; i < interactionMatrix.size(); ++i){
    row_len[i] = interactionMatrix.getRow(i).size();
    max_row_len = max(row_len[i],max_row_len);
  }


  clk.start("Eigensolver");
  EPS eps;
  Mat A;
  Vec eigen_vec_re, eigen_vec_im;
  PetscScalar eigen_val_re, eigen_val_im;
  PetscInt nconv,its;
  PetscInt nev = n_eigvec;
//   PetscInt ncv = nev*3;
//   PetscInt mpd = 3000;
  PetscErrorCode ierr;

  cout << "  Call to Eigensolver:" << endl<< flush;

  cout << "  > Initializing..." << endl<< flush;
  int argc = 1;
  char xx1[] = "1"; // Now these are null-terminated strings
  char xx2[] = "2";
  char* fake_argv[] = {xx1, xx2};
  char** argvp = fake_argv; // Use the fact that the array will decay
  sprintf(xx1,"%d",call_number);
  ierr = SlepcInitialize(&argc,&argvp,(char*)0,help);CHKERRQ(ierr);

  
  /* EP NON-SIMMETRICI */
// Nel caso di un problema non-simmetrico, basta cambiare HEP
// in NHEP nel EPSSetProblemType( eps, EPS_HEP ).
  /* EP SIMMETRICI*/
// Per usare solo mezza matrice nei problemi simmetrici,
// invece di
// MatCreateSeqAIJ(PETSC_COMM_WORLD,dim,dim,max_row_len,row_len,&A
// usare la routine
// MatCreateSeqSBAIJ(MPI_Comm comm,PetscInt bs,PetscInt m,PetscInt n,PetscInt nz,const PetscInt nnz[],Mat *A);
// i.e.
// MatCreateSeqSBAIJ(PETSC_COMM_WORLD,dim,dim,dim,max_row_len,row_len,&A);
// La matrice in input deve essere riempita solo nel triangolo superiore.
// 
// Le uniche matrici simmetriche che puo' usare SLEPc sono a
// blocchi: basta allora creare una matrice simmetrica con un unico blocco
// grande quanto la matrice.

  cout << "  > Preparing Matrix..." << endl<< flush;
  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD,dim,dim,max_row_len,row_len,&A);CHKERRQ(ierr);
  for (size_t i = 0; i < interactionMatrix.size(); ++i){
    for (size_t j = 0; j < interactionMatrix.getRow(i).size(); ++j){
      PetscInt jidx = interactionMatrix.getRow(i).at(j).columnIndex;
      PetscScalar val = interactionMatrix.getRow(i).at(j).value;
      MatSetValue(A,jidx,i,val,INSERT_VALUES);
    }
  }

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  //MatSetOption(A, MAT_SYMMETRIC);
  ierr = MatGetVecs(A,0,&eigen_vec_im);CHKERRQ(ierr);
  ierr = MatGetVecs(A,0,&eigen_vec_re);CHKERRQ(ierr);

  cout << "  > Preparing eigenproblem..." << endl<< flush;
  ierr = EPSCreate( PETSC_COMM_WORLD, &eps );CHKERRQ(ierr);
  ierr = EPSSetOperators( eps, A, 0 );CHKERRQ(ierr);
  ierr = EPSSetProblemType( eps, EPS_NHEP );CHKERRQ(ierr);
  ierr = EPSSetDimensions( eps, nev, PETSC_DECIDE, PETSC_DECIDE);CHKERRQ(ierr);
  ierr = EPSSetType( eps, EPSKRYLOVSCHUR );CHKERRQ(ierr);
  
  ierr = EPSSetWhichEigenpairs( eps, EPS_SMALLEST_MAGNITUDE);CHKERRQ(ierr);
  // oppure:
//   ierr = EPSSetWhichEigenpairs( eps, EPS_TARGET_MAGNITUDE);CHKERRQ(ierr);
  ST st;
  EPSGetST(eps,&st);
  STSetType(st, STSINVERT);
  EPSSetTarget(eps, -0.0001);


  ierr = EPSSetFromOptions( eps );CHKERRQ(ierr);
  cout << "  > Solving eigenproblem..." << endl<< flush;
  ierr = EPSMonitorSet(eps,EPSMonitorFirst,0,0);CHKERRQ(ierr);
  ierr = EPSSolve( eps );CHKERRQ(ierr);
  cout << "  > Done." << endl<< flush;
  ierr = EPSGetConverged( eps, &nconv );CHKERRQ(ierr);
  ierr = EPSGetIterationNumber( eps, &its );CHKERRQ(ierr);

  cout << "  #### First 15 eigenvalues ####   "<< endl;
  cout << "  "  << nconv << "  converged eigenvectors in " << its << " iterations." << endl<< flush;
  cout << "\t" << setw(3) << "#" << setw(15) << "Re(l)" << setw(15) << "Im(l)"
       << setw(15) << "Residual" << endl<< flush;


  // print outputs
  char evalfname[256],evecfname[256];
  sprintf(evalfname,"if [ ! -d results/eigenspace-%d ]; then mkdir results/eigenspace-%d; fi",iter,iter);
  system(evalfname);
  
  sprintf(evalfname,"results/eigenspace-%d/eigenvalues.dat",iter);
  FILE* evalf = fopen(evalfname,"w");
  
  PetscScalar* x = (PetscScalar*)malloc(dim);
  double eigval_old = 0.;
  double* sparsity_l4 = (double*)malloc((size_t) prot_len*sizeof(double));
  int* sparsity_l0 = (int*)malloc((size_t) prot_len*sizeof(int));
  int* sparsity_max = (int*)malloc((size_t) prot_len*sizeof(int));
  int* not_outlier_tmp = (int*)malloc((size_t) prot_len*sizeof(int));
  int check_eval=1,num_zero_eigvals=0;
  for(int i=0; i<dim; i++) {
    sparsity_l4[i] = 0.;
    sparsity_l0[i] = 0;
    sparsity_max[i] = 0;
  }

  for (PetscInt j=0; j < nconv; ++j) {
    PetscReal res_norm;
    ierr = EPSGetEigenpair( eps, j,&eigen_val_re,&eigen_val_im,eigen_vec_re, eigen_vec_im);CHKERRQ(ierr);
    ierr = EPSComputeResidualNorm( eps, j, &res_norm);CHKERRQ(ierr);
    if(j<15) cout << "\t" << setw(3) << j+1 << setw(15) << eigen_val_re
                  << setw(15) << eigen_val_im << setw(15) << res_norm << endl<< flush;

    // output: eigenvalues
    fprintf(evalf,"%d %e %e \n",j, eigen_val_re, eigen_val_re-eigval_old);
    eigval_old = eigen_val_re;
    if(eigen_val_re>1.) check_eval=0;
    if(eigen_val_re<1.e-9) ++num_zero_eigvals;

    // output: eigenvectors (and sparsity info on them)
    VecGetArray(eigen_vec_re,&x);
    if(j < n_dom_max) {
      // print eigvecs on file
      sprintf(evecfname,"results/eigenspace-%d/eigenvector_%d.dat",iter,j);
      FILE* evecf = fopen(evecfname,"w");
      for (PetscInt k=0; k<dim; ++k) fprintf(evecf,"%d %le\n",k,x[k]);      
      fclose(evecf);
    }
    int cont=0;
    for (PetscInt k = 0; k < dim; ++k) {
      if(j<n_dom_max) first_eigvecs[k][j]=x[k];
      if(j < dim) {
	// compute sparsity for each residue
	if(eigen_val_re<1.) sparsity_l4[k] += pow(x[k],4); // oppure:
// 	sparsity_l4[k] += pow(x[k],4)*exp(-eigen_val_re);
	if(fabs(x[k])>0.001) sparsity_l0[k]++;
	if(fabs(x[k])>0.65) sparsity_max[k]=1;
	// count non-zero components
	if(fabs(x[k])>0.001) {
	  cont++;
	  not_outlier_tmp[k] = 0;
	}
	else not_outlier_tmp[k] = 1;
      }
    }
//     if(cont<=6 && cont!=0) for(int i=0; i<dim; i++) not_outlier_out[i]*=not_outlier_tmp[i];
  }
  fclose(evalf);
  
  // find the outliers - compute the l4 norm, its mean and variance
  int kount=0;
  double l4_mean=0., l4_var=0., temp;
  for(int i=0; i<dim; i++) {
    if(not_outlier_in[i]) {
      temp = pow(sparsity_l4[i],0.25);
      sparsity_l4[i] = temp;
      ++kount;
      l4_mean += temp;
      l4_var += temp*temp;
    }
    else sparsity_l4[i] = 0.;
  }
  l4_var = l4_var/(kount-1) - l4_mean*l4_mean/(kount*(kount-1));
  l4_mean = l4_mean/kount;
  // outliers have l4_norm higher than (mean + 2 var)
  temp = l4_mean + 2*sqrt(l4_var);
  for(int i=0; i<dim; i++) {
    if(not_outlier_in[i] && sparsity_l4[i]>temp) not_outlier_out[i]=0;
  }
  
  sprintf(evecfname,"results/eigenspace-%d/eigenvectors_sparsity.dat",iter);
  FILE* evecf = fopen(evecfname,"w");  
  for(int i=0; i<dim; i++) {
    fprintf(evecf,"%d %f %d %d %lf\n",i,
	    sparsity_l4[i],sparsity_l0[i],sparsity_max[i],
	    not_outlier_out[i]*temp);
  }
  fclose(evecf);
  
  // all eigvals<1 have been found?
  if(check_eval) fprintf(stderr,"# WARNING: maybe not all eigvals < 1 have been found...\n");
  printf("  Number of eigenvalues equal to zero = %d.\n",num_zero_eigvals);


  // destroy workspace
  ierr = EPSDestroy(&eps);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_im);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_re);CHKERRQ(ierr);
  ierr = SlepcFinalize();
//   if(call_number==2) ierr = SlepcFinalize();

  cout << "  Timings:" << endl<< flush;
  printf("  > ");
//   clk.print("Laplacian");
//   printf("  > ");
  clk.print("Eigensolver");
  printf("  > ");
  clk.print("Total");
  
  free(sparsity_l4);
  free(sparsity_l0);
  free(sparsity_max);
  free(not_outlier_tmp);

  printf("\n");
  return num_zero_eigvals;
}
