/*
 * SparseMatrix.h
 *
 *  Created on: Jul 10, 2012
 *      Author: gpolles
 */

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <vector>

namespace betagm
{

typedef float real_t ;

struct CscSparseMatrix{
  int dimension; /* dimension of the matrix */
  int numNonZero; /* number of nonzero values */
  std::vector<int> irows; /* index of the rows */
  std::vector<int> pcols; /* index at which a new column start */
  std::vector<real_t> values; /* array of values */

};

class SparseMatrixElement{
public:
  SparseMatrixElement() : rowIndex(0), value(0) {}
  SparseMatrixElement(size_t j) : rowIndex(j), value(0) {}
  SparseMatrixElement(size_t j, real_t val) : rowIndex(j), value(val) {}
  size_t rowIndex;
  real_t value;
};

inline bool customSortingFunction(const SparseMatrixElement a, const SparseMatrixElement b) {
      return (a.rowIndex<b.rowIndex);
}



inline int find(size_t a, const std::vector<SparseMatrixElement>& v){
  for (size_t i = 0; i < v.size(); ++i) {
    if(a==v[i].rowIndex) return static_cast<int>(i);
  }
  return -1;
}

class SparseMatrix
{
public:
  SparseMatrix() : _size(0), _numNonZero(0){}
  SparseMatrix(size_t size) : _size(size), _numNonZero(0){
    _columns.resize(size);
  }
  virtual ~SparseMatrix() {}

  real_t& operator() (size_t i, size_t j){
    int pos=find(i,_columns[j]);
    if(pos==-1){
      _columns[j].push_back(SparseMatrixElement(i,0));
      ++_numNonZero;
      return _columns[j].back().value;
    }
    return _columns[j][pos].value;
  }
  void getCsc(struct CscSparseMatrix& csc);
  bool isDefined(size_t i, size_t j){
    int pos=find(i,_columns[j]);
    if(pos==-1){
      return false;
    }
    return true;
  }

  std::vector<SparseMatrixElement>& getColumn(size_t i){
    return _columns[i];
  }

  void print();

  size_t getNumNonZero() const {
    return _numNonZero;
  }

  size_t getSize() const {
    return _size;
  }

  void setSize(size_t size){
    for (size_t i = _size; i > size; --i) {
      _numNonZero -= _columns[i-1].size();
    }
    _columns.resize(size);
    _size = size;
  }

  void clear(){
    _size =0;
    _numNonZero = 0;
    _columns.resize(0);
  }

protected:
  std::vector<std::vector<SparseMatrixElement> > _columns;
  size_t _size;
  size_t _numNonZero;
};

} /* namespace betagm */
#endif /* SPARSEMATRIX_H_ */
