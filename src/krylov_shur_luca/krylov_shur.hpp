int krylov_shur(int n_dom_max, int prot_len, int n_eigvec, 
		      mylib::SparseMatrix<double>& interactionMatrix, vector< vector<double> >& first_eigvecs,
		      vector<int>& not_outliers_in, vector<int>& not_outliers_out, int iter);
