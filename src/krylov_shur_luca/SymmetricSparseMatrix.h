/*
 * SymmetricSparseMatrix.h
 *
 *  Created on: Jul 9, 2012
 *      Author: gpolles
 */

#ifndef SYMMETRICSPARSEMATRIX_H_
#define SYMMETRICSPARSEMATRIX_H_

#include <vector>
#include "SparseMatrix.h"

namespace betagm{







class SymmetricSparseMatrix : public SparseMatrix
{
public:
  /*SymmetricSparseMatrix() : _size(0), _numNonZero(0){}
  SymmetricSparseMatrix(size_t size) : _size(size), _numNonZero(0){
    _columns.resize(size);
  }
  virtual ~SymmetricSparseMatrix() {}
   */
  SymmetricSparseMatrix(SparseMatrix& m){
    _columns.resize(m.getSize());
    _size = m.getSize();
    _numNonZero = 0;
    for (size_t j = 0; j < m.getSize(); ++j) {
      for (size_t k = 0; k < m.getColumn(j).size(); ++k) {
        if (m.getColumn(j)[k].rowIndex >= j){
          _columns[j].push_back(m.getColumn(j)[k]) ;
          ++_numNonZero;
        }
      }
    }
  }
  real_t& operator() (size_t i, size_t j){
    if (j>i) std::swap(i,j);
    return SparseMatrix::operator ()(i,j);
  }
  bool isDefined(size_t i, size_t j){
    if (j>i) std::swap(i,j);
    return SparseMatrix::isDefined(i,j);
  }


};

}/*namespace betagm*/


#endif /* SYMMETRICSPARSEMATRIX_H_ */
