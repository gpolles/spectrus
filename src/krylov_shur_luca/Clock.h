/*
 * Clock.h
 *
 *  Created on: May 27, 2013
 *      Author: gpolles
 */

#ifndef CLOCK_H_
#define CLOCK_H_

#include <ctime>
#include <vector>
#include <string>

struct clk_t{
	time_t start;
	time_t end;
	bool running;
	std::string label;
};

class Clock {
public:
	Clock();
	virtual ~Clock();

	void start(const std::string& label);
	void stop(const std::string& label);
	void print(const std::string& label, std::ostream& stream = std::cout);
	long int getTime(const std::string& label);

private:
	int find(const std::string& label) const;

private:
	std::vector<clk_t> _clocks;
};

#endif /* CLOCK_H_ */

