void failed (char *message);

int *i1t (int n1);

int **i2t (int n1, int n2);

double **d2t (int n1, int n2);

double *d1t (int n1);

double ***d3t (int n1, int n2, int n3);

void free_i1t(int *p);

void free_i2t(int **p);

void free_d1t(double *p);

void free_d2t(double **p);

void free_d3t(double ***p);