#include <stdio.h>
#include <stdlib.h>


void failed (char message[]) {
  
  fprintf (stderr,"Error. \nFAILED *** %s *** \n\n", message);
  exit (1);
  
}


void checkpoint (char message[]) {
  
  printf ("\nCHECKPOINT *** %s ***\n \n", message);

}
