
#ifndef __KRYLOV_SHUR_HPP__
#define __KRYLOV_SHUR_HPP__

#include "../mylib/sparsematrix.hpp"

int krylov_shur(mylib::SymmetricSparseMatrix<double>& input_matrix,
		int n_eigvec,
		int* n_converged,
		std::vector< vector<double> >& eigvec,
		std::vector<double>& eigval);

#endif
