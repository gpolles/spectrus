//============================================================================
// Name        : krylov_shur.cpp
// Author      : Guido Polles
// Version     :
// Copyright   : 
// Description : 
//============================================================================

#include <mpi.h>

#include <slepceps.h>

#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>

#include "../mylib/sparsematrix.hpp"

typedef float real_t;

using namespace std;

static char help_slepc[] = "help msg";
static char help[] = "Usage: krylov_shur [slepc options] <matrix filename> <number of eigenvectors>\n";

int main(int argc,char** argv) {

  if(argc < 3){
    cerr << help;
    exit(1);
  }
  int n_eigvec=-1;
  char* n_eigvec_str = argv[argc-1];
  sscanf(n_eigvec_str,"%d",&n_eigvec);
  argc--;
  char* fname = argv[argc-1];
  argc--;
    
  
  // read matrix:
  mylib::SymmetricSparseMatrix<real_t> input_matrix;
  input_matrix.readFromFile(fname);
  
  // SLEPc initialize
  
  
  // Get problem size and row lenghts for faster allocation. 
  PetscInt dim = input_matrix.size();
  PetscInt max_row_nnz = 0;
  PetscInt row_nnz[dim];
  for (int i = 0; i < dim; ++i){
    row_nnz[i] = input_matrix.getRow(i).size();
    max_row_nnz = max(row_nnz[i],max_row_nnz);
  }
  
  PetscErrorCode  ierr;
  ierr = SlepcInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  
  // Set up the matrix
  Mat A;
  ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,dim,dim);CHKERRQ(ierr);
  ierr = MatSetType(A, MATSEQSBAIJ);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(A, max_row_nnz, row_nnz);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  // fill matrix
  PetscInt start,end;
  MatGetOwnershipRange(A,&start,&end);
  for(PetscInt row = start; row < end; row++) {
    int nnz=row_nnz[row];
    PetscInt    idxj[nnz];
    PetscScalar vals[nnz];
    cout << "row:" << row << endl;
    for (size_t j = 0; j < nnz; ++j){
      idxj[j] = input_matrix.getRow(row).at(j).columnIndex;
      vals[j] = input_matrix.getRow(row).at(j).value;
      cout << idxj[j] << "\t";
    }
    cout << endl;
    MatSetValues(A,1,&row,nnz,idxj,vals,INSERT_VALUES);
  }

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    
  // Solve the EP with SLEPc
  
  EPS eps;
  Vec eigen_vec_re, eigen_vec_im;
  PetscScalar eigen_val_re, eigen_val_im;
  PetscInt nconv,its;
  PetscInt nev = n_eigvec;

  cout << "  Call to Eigensolver:" << endl<< flush;
  
  ierr = MatGetVecs(A,0,&eigen_vec_im);CHKERRQ(ierr);
  ierr = MatGetVecs(A,0,&eigen_vec_re);CHKERRQ(ierr);

  cout << "  > Preparing eigenproblem..." << endl<< flush;
  ierr = EPSCreate( PETSC_COMM_WORLD, &eps );CHKERRQ(ierr);
  ierr = EPSSetOperators( eps, A, 0 );CHKERRQ(ierr);
  ierr = EPSSetProblemType( eps, EPS_HEP );CHKERRQ(ierr);
  ierr = EPSSetDimensions( eps, nev, PETSC_DEFAULT, PETSC_DEFAULT);CHKERRQ(ierr);
  ierr = EPSSetType( eps, EPSKRYLOVSCHUR );CHKERRQ(ierr);
  
  ierr = EPSSetWhichEigenpairs( eps, EPS_SMALLEST_MAGNITUDE);CHKERRQ(ierr);

  ierr = EPSSetFromOptions( eps );CHKERRQ(ierr);
  cout << "  > Solving eigenproblem..." << endl<< flush;
  ierr = EPSMonitorSet(eps,EPSMonitorFirst,0,0);CHKERRQ(ierr);
  ierr = EPSSolve( eps );CHKERRQ(ierr);
  
  cout << "  > Done." << endl<< flush;
  
  // get results and print
  ierr = EPSGetConverged( eps, &nconv );CHKERRQ(ierr);
  ierr = EPSGetIterationNumber( eps, &its );CHKERRQ(ierr);

  cout << "  #### First 15 eigenvalues ####   "<< endl;
  cout << "  "  << nconv << "  converged eigenvectors in " << its << " iterations." << endl<< flush;
  cout << "\t" << setw(3) << "#" << setw(15) << "Re(l)" << setw(15) << "Im(l)"
       << setw(15) << "Residual" << endl<< flush;

  for (PetscInt j=0; j < nconv && j < 15 ; ++j) {
    PetscScalar res_norm;
    ierr = EPSGetEigenpair( eps, j,&eigen_val_re,&eigen_val_im, eigen_vec_re, eigen_vec_im);CHKERRQ(ierr);
    ierr = EPSComputeResidualNorm( eps, j, &res_norm);CHKERRQ(ierr);
    cout << "\t" << setw(3) << j+1 << setw(15) << eigen_val_re
        << setw(15) << eigen_val_im << setw(15) << res_norm << endl<< flush;
  }

  // save outputs
  char evalfname[256],evecfname[256];
  system(evalfname);
  sprintf(evalfname,"%s_eigenvalues.dat",fname);
  sprintf(evecfname,"%s_eigenvectors.dat",fname);
  FILE* evalf = fopen(evalfname,"w");
  FILE* evecf = fopen(evecfname,"w");

  for (PetscInt j=0; j < nconv; ++j) {
      PetscReal res_norm;
      ierr = EPSGetEigenpair( eps, j,&eigen_val_re,&eigen_val_im,eigen_vec_re, eigen_vec_im);CHKERRQ(ierr);
      ierr = EPSComputeResidualNorm( eps, j, &res_norm);CHKERRQ(ierr);
      
      // output: eigenvalues
      PetscScalar* x = (PetscScalar*)malloc(dim);
      VecGetArray(eigen_vec_re,&x);
      fprintf(evalf,"%.6e\n",eigen_val_re);
      for (PetscInt k=0; k<dim; ++k) {
	fprintf(evecf, "%.6e ", x[k]);      
      }
      fprintf(evecf,"\n");
  }
  fclose(evecf);
  fclose(evalf);

  // destroy workspace
  ierr = EPSDestroy(&eps);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_im);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_re);CHKERRQ(ierr);
  ierr = SlepcFinalize();
  
  printf("\n");
  return 0;
}
