// LAUNCH WITH:
// ./main.out <trajectory file> 
// OR:
// ./main.out <pdb file> <num normal modes> <num decimation>:
//
// INPUT (launch options)
// - a trajectory file (or a set of conformations), in .xyz form
// - alternatively, a pdb file for the ENM. In this case, the number of modes and the decimation number must be
//   provided as well, and the pdb file _has_to_be_prepared_already_decimated_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <exception>
#include <map>
#include <math.h>
#include <numeric>
#include <ctime>

#include <mylib/mylib.h>

using namespace std;
using namespace mylib;

#include "krylov_shur_luca/krylov_shur.hpp"			/* Routine for solving eig. problem for sparse matrices, using SLEPc */
#include "my_malloc.h"  					/* Routines for memory allocation */
#include "modes_to_fluct_matrix/modes_to_fluct_matrix.hpp"	/* for computing the distance fluctuation matrix from ENM */
#include "k_medoids/k_medoids.hpp"				/* Routine which performs k-medoids on a set of points */

typedef vector<vector<double> > v2d_t;
typedef std::map<std::string,std::string> dict;

int read_last_frame(const char* fname, int N_ATOMS, std::vector<mylib::Vector3d>& pos_atom, int* n_frames_p);
double compute_median(std::vector<double>& vec);
dict read_parameters(const char* fname);
void compute_Q_dist_matrix(int N_ATOMS, int q_max, double** x, double **mtrx_sigma, double *sigma_max_p);
void find_sign_groups(int N_ATOMS, int Q, double** x, int* groups);

template <typename T>
std::vector<T> flatten(const std::vector<std::vector<T> >& v) {
    std::size_t total_size = 0;
    for (const auto& sub : v)
      total_size += sub.size();
    std::vector<T> result;
    result.reserve(total_size);
    for (const auto& sub : v)
      result.insert(result.end(), sub.begin(), sub.end());
    return result;
}



/// MAIN ///

int main(int argc, char* argv[])
{
  if(argc<2 || argc==3 || argc>4){
    cerr << "  Usage: ./main.out <trajectory file>" << endl;
    cerr << "     or: ./main.out <pdb file> <num normal modes> <num decimation>" << endl;
    return 1;
  }

  char* fname = argv[1]; // name of trajectory file or pdb file
  int n_modes, decimation;
  if(argc==2) {
    cout << "  Distance fluctuation matrix computed from the file: " << endl;
    cout << "  " << argv[1] << endl << endl;
  }
  if(argc==4) {
    n_modes  = std::atoi(argv[2]);
    decimation = std::atoi(argv[3]);
    cout << "  Distance fluctuation matrix computed from ENM." << endl;
    cout << "  Number of modes: "   << n_modes  << endl;
    cout << "  Decimation number: " << decimation << endl << endl;
  }

  int n_atoms, max_dom, min_dom, kmed_iter;
  double CUTOFF = 0.;
  double tmp;
  FILE *fp;

  // read parameters from file
  dict params = read_parameters("INPUT_PARAMS.DAT");
  try{
    n_atoms   = atoi(params.at("N_ATOMS"));
    CUTOFF    = atof(params.at("CUTOFF"));
    max_dom   = atoi(params.at("N_DOM_MAX"));
    min_dom   = atoi(params.at("N_DOM_MIN"));
    kmed_iter = atoi(params.at("KMED_ITER"));
  } catch (exception& e){
    cerr << "  Some options missing in parameter file." << endl;
    cerr << e.what() << endl;
    return 1;
  }


  // vector for atom positions
  vector<mylib::Vector3d> pos_atom(n_atoms);
  
  // distance fluctuation matrix
  vector< vector<double> > fluct(n_atoms);


  // read last frame and check for the number of frames
  int n_frames = 0;
  read_last_frame(fname, n_atoms, pos_atom, &n_frames);
   
  /***********************/
  // read the FIRST FRAME (for comparison with the old program)
  ifstream fin2(fname);
  for (int i=0; i<n_atoms; ++i){
    fin2 >> pos_atom[i];
  }
  fin2.close();
  /************************/
  
  // build contact map
  mylib::ContactMap cm(pos_atom,CUTOFF); 
  
  
  if(argc==2) { /* distance fluctuation matrix from a set of conformations (trajectory) */

    // prepare vectors for fluctuation calculation
    vector<vector<double> > dist(n_atoms);
    vector<vector<double> > dist_sq(n_atoms);
    for (int i=0; i<n_atoms; ++i){
      dist[i].resize(   cm[i].size(),0.0);
      dist_sq[i].resize(cm[i].size(),0.0);
      fluct[i].resize(  cm[i].size(),0.0);
    }

    // loop over frames
    ifstream fin(fname);
    for(int frame = 0; frame<n_frames; ++frame){
      // read atom positions
      for (int i=0; i<n_atoms; ++i){
        fin >> pos_atom[i];
      }
      // compute distances between atoms in the contact map
      for (int i=0; i<n_atoms; ++i){
        for (int j=0; j<cm[i].size(); ++j){
          double dist2 = mylib::distanceSQ(pos_atom[i],pos_atom[ cm[i][j] ]);
          dist_sq[i][j] += dist2;
          dist[i][j] += sqrt(dist2);
        }
      }
    }
    fin.close();

    // compute distance fluctuations between atoms in the contact map
    for (int i=0; i<n_atoms; ++i){
      for (int j=0; j<cm[i].size(); ++j){
        fluct[i][j] = sqrt( (dist_sq[i][j]/(n_frames-1)) - mylib::SQ(dist[i][j])/n_frames/(n_frames-1) );
      }
    }

    // no longer need dist and dist2, free space
    vector<vector<double> >().swap(dist);
    vector<vector<double> >().swap(dist_sq);
  }
  else { /* distance fluctuation matrix from ENM */

    // prepare the distance fluctuation matrix
    for (int i=0; i<n_atoms; ++i){
      fluct[i].resize( cm[i].size(),0.0 );
    }
    
    // compute the distance fluctuation matrix from normal modes
    ModesToFluctMatrix(fname, n_atoms, decimation, n_modes, cm, fluct);
  }
  
 
  // compute the median
  double median;
  vector<double> flatten_fluct = flatten(fluct);
  median = compute_median(flatten_fluct);
  vector<double>().swap(flatten_fluct);
  cout << "  Median of distance fluctuations in the contact map: " << median << endl << endl;
  
  // compute the MEAN of distance fluctuations (for comparison with the old program)
  // and replace median with mean
  // (RICHIEDE LA LIBRERIA NUMERIC)
  flatten_fluct = flatten(fluct);
  double sum = std::accumulate(flatten_fluct.begin(), flatten_fluct.end(), 0.0);
  double mean = sum / flatten_fluct.size();
  vector<double>().swap(flatten_fluct);
  cout << "  Mean of distance fluctuations in the contact map: " << mean << endl << endl;
  median = mean;
  
  // generate similarity matrix
  vector< vector<double> > sim_matrix(n_atoms);
  for (int i=0; i<n_atoms; ++i) sim_matrix[i].resize(cm[i].size(),0.0);
  tmp = 2.*pow(median,2);
  for (int i=0; i<n_atoms; ++i){
    for (int j=0; j<cm[i].size(); ++j){
      sim_matrix[i][j] = exp(-pow(fluct[i][j],2)/tmp) ;
    }
  }
  
  // compute the sum of each row (degree matrix)
  vector<double> diag(n_atoms,0.0);
  for (int i=0; i<n_atoms; ++i){
    for (int j=0; j<cm[i].size(); ++j){
      diag[i] += sim_matrix[i][j];
    }
  }
  
  // check diagonal consistency
  for (int i=0; i<n_atoms; ++i){
    if(diag[i] <= 0.) {
      fprintf(stderr, "# ERROR! Null row detected in the sparse matrix. \n");
      fprintf(stderr, "# ERROR! Row %d has only zeroes. Stopped. \n", i);
      exit(1);
    }
  }
  
  // build the SYMMETRIC LAPLACIAN L_sym = I - D^{-1/2} S D^{-1/2}
  mylib::SparseMatrix<double> L_sym(n_atoms);
  for(int i=0; i<n_atoms; ++i) {
    L_sym.insert( i, i, 1. );
    for(int cj=0; cj<cm[i].size(); ++cj) {
      size_t j = cm[i][cj];
      L_sym.insert( i, j, -sim_matrix[i][cj] / sqrt(diag[i]*diag[j]) );
    }
  }
  

  // define vectors for storing Laplacian's eigenvectors 
  vector< vector<double> > first_eigvecs(n_atoms);
  for (int i=0; i<n_atoms; ++i){
    first_eigvecs[i].resize(max_dom);
  }
  
  // no outliers...
  vector<int> not_outlier_aux(n_atoms);
  for(int i=0; i<n_atoms; ++i) not_outlier_aux[i]=1;
  
  // create "/results" directory
  system("rm -rf results; mkdir results");
  // system("if [ ! -d results ]; then mkdir results; fi");
  
  // spectral analysis of the Laplacian of the similarity matrix.
  int nev=max_dom+2;
  if(nev<max_dom+2) nev=max_dom+2;
  if(nev>n_atoms) nev=n_atoms;
  int num_zero_eigvals = krylov_shur(max_dom,n_atoms,nev,L_sym,first_eigvecs,
			 not_outlier_aux,not_outlier_aux,1);



  /************ cycle on Q (number of domains) *************/
  
  double **first_eigvecs_norm = d2t(n_atoms,max_dom);
  int *groups = i1t(n_atoms);
  int *label_kmed = i1t(n_atoms);
  double q_score_median, q_score_mean;
  char filename[256],command[100];
  
  
  for(int q=min_dom; q<=max_dom; ++q) {
 
    /*** k-MEDOIDS CLUSTERIZATION ***/
    
    // normalization of the points on the unitary Q-dimensional sphere
    int skip_k_medoids = 0;
    for(int i=0; i<n_atoms; ++i) {
      double norm = 0.;
      for(int j=0; j<q; ++j) norm+=pow(first_eigvecs[i][j],2);
      norm = sqrt(norm);
      if(norm == 0.) { 
	++skip_k_medoids;
	continue;
      }
      for(int j=0; j<q; ++j) first_eigvecs_norm[i][j]=first_eigvecs[i][j]/norm;
    }
    if(skip_k_medoids) {
      fprintf(stderr, "# WARNING: Some eigenvectors of the kernel space are missing! \n");
      fprintf(stderr, "# WARNING: K-medoids for Q = %d will be skipped. \n",q);
      fprintf(stderr, "# WARNING: (number of atoms with all components equal to zero = %d.) \n", skip_k_medoids);
      continue;
    }
    

    // print the coordinates of the points on the Q-hypersphere
    fp = fopen("k_medoids_coordinates.dat","w");
    for(int i = 0; i < n_atoms; ++i) {
      for(int j = 0; j < q; ++j) {
	fprintf(fp,"%le ",first_eigvecs_norm[i][j]);
      }
      fprintf(fp,"\n");
    }
    fclose(fp);
          
    // find atoms with the same sequence of signs in the eigvectors
    if(num_zero_eigvals <= 1) find_sign_groups(n_atoms, q, first_eigvecs_norm, groups);

    time_t now = time(0);
    tm* localtm = localtime(&now);
    cout << "kmedoids started on: " << asctime(localtm) << endl;
    k_medoids(n_atoms, q, q, 1, q, kmed_iter);
    now = time(0);
    localtm = localtime(&now);
    cout << "kmedoids started on: " << asctime(localtm) << endl;
    
    // import the cluster profile (labels)
    sprintf(filename, "k_medoids_output/output_k_%d/cluster_profile.dat",q);
    fp = fopen(filename,"r");
    for(int i = 0; i < n_atoms; ++i) {
      fscanf(fp, "%*d \t%d \n", &label_kmed[i]);
      if(label_kmed[i]<0) label_kmed[i] = -1;
    }
    fclose(fp);

    // import the quality score
    fp = fopen("k_medoids_output/quality_score.dat","r");
    fscanf(fp,"%*d \t%le \t%le\n",&q_score_median,&q_score_mean);
    fclose(fp);
 
    // move the directory into results
    sprintf(command, "mv k_medoids_output results/k_medoids_output_%d", q);
    system(command);
    system("rm -f k_medoids_coordinates.dat k_medoids_sign_groups.dat");
    

    /*** PRINT THE RESULTS ***/
    
    fp = fopen("results/quality_score.dat","a");
    fprintf(fp, "%4d %le %le\n", q, q_score_median, q_score_mean);
    fclose(fp);

    
    // prints the final clusterization (in .pdb format)
    sprintf(filename, "results/final_clusterization_kmed-%d.pdb", q);
    fp = fopen(filename,"w");
    sprintf(filename, "results/final_labels_kmed-%d.dat", q);
    FILE *fp_labels = fopen(filename,"w");
    for(int j = 0; j < n_atoms; ++j) ++label_kmed[j];
    for(int j = 0; j < n_atoms; ++j) {
      int i = j + 1;
      fprintf(fp,
	      "ATOM %6d  CA  GLY A%4d%12.3lf%8.3lf%8.3lf  1.00%3d.00\n",
	      i,i,pos_atom[j][0],pos_atom[j][1],pos_atom[j][2],label_kmed[j]);
      fprintf(fp_labels, "%d %d \n",j,label_kmed[j]);
    }
    fclose(fp);
    fclose(fp_labels);
    
  }

  free_d2t(first_eigvecs_norm);
  free_i1t(label_kmed);
  free_i1t(groups);

  return 0;
}


/******************************/
/***** SUBROUTINES /************/
/******************************/


int read_last_frame(const char* fname, int N_ATOMS, 
                    std::vector<mylib::Vector3d>& pos_atom, int* n_frames_p){  // very dumb way to do it, but whatever

  FILE *fp = fopen(fname,"r");
  int n_frames,exit_flag=0;

  for(n_frames=0; ; ++n_frames) {
    // read one conformation (or frame) from the file
    for(int i=0; i<N_ATOMS; ++i) {
      if(fscanf(fp,"%lf %lf %lf",&pos_atom[i][0],&pos_atom[i][1],&pos_atom[i][2] )==EOF) {
	// check for a correct EOF
	if(i==0) {
	  exit_flag = 1;
	  break;
	}
	else {
	  fprintf(stderr, "# ERROR in reading conformation file!\n");
	  exit(1);
	}
      }
    }
    if(exit_flag) break;
  }
  fclose(fp);
  *n_frames_p = n_frames;

  return 0;
}


/******************************/


double compute_median(std::vector<double>& vec){

  std::vector<double> v = vec;
  std::sort(v.begin(),v.end());

  if( v.size()%2==0 ) return (v[v.size()/2] + v[v.size()/2-1]) /2.0;
  else return v[v.size()/2];
}


/******************************/


dict read_parameters(const char* fname){

  dict params;

  ifstream fin(fname);
  if (!fin.good()){
    cerr<< "# Cannot read parameter file" << endl;
    exit(1);
  }
  string key,val;
  while(!(fin >> key >> val).fail()) params[key]=val;

  return params;
}


/******************************/


void compute_Q_dist_matrix(int N_ATOMS, int q_max, 
			   double** x, double **mtrx_sigma, double *sigma_max_p) {

  double sigma_max,tmp;

  printf("  DISTANCE MATRIX FROM TOP %d EIGENVECTORS \n", q_max);
  printf("  ---------------------------------------- \n");

  sigma_max = 0.;
  for(int i=0; i<N_ATOMS; ++i) {
    for(int j=i+1; j<N_ATOMS; ++j) {
      tmp = 0.;
      // distance on a unit hypersphere
      for(int q=0; q<q_max; ++q) tmp+=x[i][q]*x[j][q];
      if(tmp>1.) tmp=0.;
      else if(tmp<-1.) tmp=3.1415926535;
      else tmp = acos(tmp);
      // OR euclidean distance
//       for(int q=0; q<q_max; ++q) tmp+=pow(x[i][q]-x[j][q],2);
//       tmp = sqrt(tmp);
      mtrx_sigma[i][j] = tmp;
      if(tmp>sigma_max) sigma_max=tmp;
    }
  }  

  // outputs
  *sigma_max_p = sigma_max;

  printf("  Computed. \n\n");
}


/******************************/


void find_sign_groups(int N_ATOMS, int Q, double** first_eigvecs, int* groups) {

  for(int i=0; i<N_ATOMS; ++i) groups[i]=-1;
  for(int i=0; i<N_ATOMS; ++i) {
    if(groups[i]==-1) groups[i]=i;
    else continue;
    for(int j=i+1,in_the_same_group; j<N_ATOMS; ++j) {
      if(groups[j]!=-1) continue;
      in_the_same_group = 1;
      for(int q=1; q<Q; ++q) {
        if((first_eigvecs[i][q]*first_eigvecs[j][q]) < 0.) {
          in_the_same_group = 0;
          break;
        }
      }
      if(in_the_same_group) groups[j]=i;
    }
  }
  FILE* fp = fopen("k_medoids_sign_groups.dat","w");
  for(int i=0; i<N_ATOMS; ++i) fprintf(fp,"%d\n",groups[i]);
  fclose(fp);

}


