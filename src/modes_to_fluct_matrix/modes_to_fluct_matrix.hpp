
// builds a correlation matrix, then a distance fluctuation
// matrix from normal modes (saved in a folder named "eigenspace/").

#include <mylib/contactmap.hpp>

int ModesToFluctMatrix(char* filename, int num_atoms, int decimation, int num_modes, mylib::ContactMap& cm,
                       std::vector< vector<double> >& fluct);