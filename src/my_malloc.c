void failed (char *message)
{
  printf ("\n FAILED *** %s ***\n \n", message);
  exit (1);
}

/**************/

int *i1t (int n1)
{
  int *p, *a;
  int i;
  char text[100];

  if ((p = (int *) malloc ((size_t) n1 * sizeof (int))) == NULL) {
    sprintf(text,"i1t: failed");
    failed (text);
  }
  for (i = 0, a = p; i < n1; i++)
    *a++ = 0;
  return p;
}

/**************/

int **i2t (int n1, int n2)
{
  int **p, *a;
  int i;
  char text[100];

  if ((p = (int **) malloc ((size_t) n1 * sizeof (int *))) == NULL) {
    sprintf(text,"i2t: failed n1");
    failed (text);
  }
  if ((p[0] = (int *) malloc ((size_t) n1 * n2 * sizeof (int))) == NULL) {
    sprintf(text,"i2t: failed n2");
    failed (text);
  }
  for (i = 0; i < n1 - 1; i++)
    p[i + 1] = p[i] + n2;
  for (i = 0, a = p[0]; i < n1 * n2; i++)
    *a++ = 0;
  return p;
}

/**************/

double **d2t (int n1, int n2)
{
  double **p, *a;
  int i;
  char text[100];

  if ((p = (double **) malloc ((size_t) n1 * sizeof (double *))) == NULL) {
    sprintf(text,"d2t: failed n1");
    failed (text);
  }
  if ((p[0] = (double *) malloc ((size_t) n1 * n2 * sizeof (double))) == NULL) {
    sprintf(text,"d2t: failed n2");
    failed (text);
  }
  for (i = 0; i < n1 - 1; i++)
    p[i + 1] = p[i] + n2;
  for (i = 0, a = p[0]; i < n1 * n2; i++)
    *a++ = 0;
  return p;
}

/**************/

double *d1t (int n1)
{
  double *p, *a;
  int i;
  char text[100];

  if ((p = (double *) malloc ((size_t) n1 * sizeof (double))) == NULL) {
    sprintf(text,"d1t: failed");
    failed (text);
  }
  for (i = 0, a = p; i < n1; i++)
    *a++ = 0;
  return p;
}

/**************/

double ***d3t (int n1, int n2, int n3)
{
  double ***p, *a;
  int i, j;
  char text[100];

  if ((p = (double ***) malloc ((size_t) n1 * sizeof (double **))) == NULL) {
      sprintf(text,"d3t: failed n1");
      failed (text);
  }
  if ((p[0] = (double **) malloc ((size_t) n1 * n2 * sizeof (double *))) == NULL) {
      sprintf(text,"d3t: failed n2");
      failed (text);
  }
  if ((p[0][0] = (double *) malloc ((size_t) n1 * n2 * n3 * sizeof (double))) == NULL) {
      sprintf(text,"d3t: failed n3");
      failed (text);
  }
  for (i = 0; i < n2 - 1; i++)
    p[0][i + 1] = p[0][i] + n3;
  for (i = 0; i < n1 - 1; i++)
    {
      p[i + 1] = p[i] + n2;
      p[i + 1][0] = p[i][0] + n2 * n3;
      for (j = 0; j < n2 - 1; j++)
	p[i + 1][j + 1] = p[i + 1][j] + n3;
    }
  for (i = 0, a = p[0][0]; i < n1 * n2 * n3; i++)
    *a++ = 0;
  return p;
}

/**************/

void free_i1t(int *p){

  free(p);
}

/**************/

void free_i2t(int **p){

  free(p[0]);
  free(p);
}

/**************/

void free_d1t(double *p){

  free(p);
}

/**************/

void free_d2t(double **p){

  free(p[0]);
  free(p);
}

/**************/

void free_d3t(double ***p){

  free(p[0][0]);
  free(p[0]);
  free(p);
}