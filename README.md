# README #

### TO DO ###

* Aggungere licenza GPL?
* memory.c e messages.c in k_medoids: usare al loro posto le librerie in include/ ?
* per spectrus.cpp, usare le librerie in include/
* idum e jdum in k_medoids.cpp e spectrus.cpp, evitare le variabili globali
* perche' questa versione e' leggermente piu' lenta di quella precedente?
* perche' il ratio_median.dat e' leggermente diverso per Q=18,20 rispetto alla versione precedente?
* includere il calcolo del prefactor

 

### What is this repository for? ###

* Spectrus: quasi-rigid domain decomposition
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
